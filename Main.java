package com.company;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        ///Activity 1
        Scanner scanner = new Scanner(System.in);

        String[] fruits = {"Apple", "Avocado", "Banana", "Kiwi", "Orange"};

        System.out.println("Please pick a fruit (Apple, Avocado, Banana, Kiwi, and Orange)");

        String search = scanner.nextLine();

        System.out.println("Index of " + search + " is : " +  Arrays.binarySearch(fruits,search));



        ///Activity 2

        ArrayList<String> names = new ArrayList<>();

        names.add("June");
        names.add("May");
        names.add("April");
        names.add("Wally");

        System.out.println("My friends are: " + names.toString());


        /// Activity 3

        HashMap hashMap = new HashMap();

        hashMap.put("toothpaste", 5);
        hashMap.put("toothbrush", 20);
        hashMap.put("soap", 12);

        System.out.println("Our current inventory consists of: " + hashMap.toString());

        String[] classroom = {};

        classroom[0][0] = "Rayquaza";
        classroom[0][1] = "Kyogre";
        classroom[0][2] = "Groudon";

        classroom[1][0] = "Sora";
        classroom[1][1] = "Goofy";
        classroom[1][2] = "Donald";

        classroom[2][0] = "Harry";
        classroom[2][1] = "Ron";
        classroom[2][2] = "Hermione";
    }
}
